// Repitition Control Structures
// Loops - lets us execute code repeatedly in a pre-set number of time or maybe forever(infinite)

// While Loop
/*
It takes in an expression/condition before proceding in the evaluation of the codes.
Syntax:
	while(expression/condition){
		statement/s;
	
	}
*/



let count = 5;

while(count !== 0){
	console.log("While Loop: " + count);

	count--; 
	// count = count - 1;
}

count = 1;

while(count <= 10){
	console.log(count)

	count++;
}


// Do-While Loop
/*
At least one code block will be executed before proceeding to the condition. 
	Syntax:
		do{
			statement/s;
		} while(expression/condition)

*/

let count3 = 5;

do{
	console.log("Do-While: " + count3);

	count3--
}while(count3 > 0);



let count4 = 1;

do{
	console.log(count4);
	count4++
}while(count4 <= 10);

// For Loop - a more flexible looping construct
/*	Syntax:
		for(initialization; expression; finalExpression/stepexpression){
			statement/s;
		}
*/

for(let count5 = 5; count5 > 0; count5--){
	console.log("For loop: " + count5);
}


let num1 = Number(prompt("Give me a number"));

for(let numCount = 1; numCount <= num1; numCount++){
	console.log("Hello batch 144");
}

let myString = "alex";
// console.log(myString.length);
// console.log(myString[2]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

// myString.length = 4
// x = 0 1 2 3 4

/*
Array
	element - represents the values in the array
	index - location of values(elements) in the array which starts at index 0.
	elements - a l e x
	index -    0 1 2 3
 
/**/

let myName = "Alex";

for(let i = 0; i < myName.length; i++){
	if
		(myName[i].toLowerCase() == 'a'||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u')
	{
		
		//if the letter in the name is a vowel, it will print the word vowel
		console.log("vowel");
	}
	else{
		// will print in the console if character is a non-vowel
		console.log(myName[i]);
		}

}

/*
Continue and Break statements
	Continue - allows the code to go to the next iteration of the loop withour finishing the execution of all statements in a code block
	Break - used to terminate the current loop once a match has been found

*/

for(let count = 0; count <= 20; count++){
	//if remainder is equal to 0 	
	if(count % 2 === 0){
		// tells the code to continue to the next iteration of the loop
		continue;
	}
	// the current value of number is printed out if the remainder is not equal to 0
	console.log("Continue and Break: " + count);

	// if the current value of count is greater than 10
	if(count > 10){
		// tells the code to terminate the loop even if the loop condition is still being satisfied.
		break;
	}
}

let name = "Alexandro";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue
	}

	if(name[i].toLowerCase() === "d"){
		break;
	}
}


